create table Movie(
    movie_id INTEGER primary key auto_increment,
    movie_title VARCHAR(30),
    movie_release_date Date,
    movie_time_hrs INTEGER,
    director_name VARCHAR(30)
);

